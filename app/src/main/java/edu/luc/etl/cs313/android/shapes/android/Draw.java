package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStrokeColor(final StrokeColor c) {
		int currColor = paint.getColor();
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setColor(currColor);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		Style currStyle = paint.getStyle();
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		paint.setStyle(currStyle);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		for (int i =0; i<g.getShapes().size(); i++) {
			g.getShapes().get(i).accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(), -l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0f, 0f,  (float) r.getWidth(), (float) r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		Style currStyle = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(currStyle);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {

		int sizeOfPts = 4*s.getPoints().size();
		final float[] pts = new float[sizeOfPts];

		int count = 0;

		for(int i=0; i<s.getPoints().size(); i++) {

			pts[count] = s.getPoints().get(i).getX();
			pts[count+1] = s.getPoints().get(i).getY();

			pts[count+2] = s.getPoints().get((i+1)%s.getPoints().size()).getX();
			pts[count+3] = s.getPoints().get((i+1)%s.getPoints().size()).getY();

			count+=4;
		}

		canvas.drawLines(pts, paint);
		return null;
	}
}
