package edu.luc.etl.cs313.android.shapes.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {

		int x =10000;
		int y =10000;
		int width = -10000;
		int height = -10000;

		for (int i=0; i<g.getShapes().size(); i++) {
			Location loc = g.getShapes().get(i).accept(this);

			Rectangle r = (Rectangle) loc.getShape();

			if(loc.getX()< x) {
				x = loc.getX();
			}
			if (loc.getY()<y) {
				y = loc.getY();
			}
			if ((r.getWidth() + loc.getX()) > width) {
				width = r.getWidth() + loc.getX();
			}
			if ((r.getHeight()+loc.getY()) > height) {
				height = r.getHeight() + loc.getY();
			}
		}

		return new Location(x, y, new Rectangle(width-x, height-y));
	}

	@Override
	public Location onLocation(final Location l) {
		Location loc = new Location(l.getX(), l.getY(), l.getShape().accept(this));
		int x = ((Location) loc.getShape()).getX() + loc.getX();
		int y = ((Location) loc.getShape()).getY()+loc.getY();
		return new Location(x, y, (Rectangle) ((Location) loc.getShape()).getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		return new Location(0, 0, new Rectangle(r.getWidth(), r.getHeight()));
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {

		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {

		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {

		int minX = 100000;
		int minY = 100000;
		int maxX = -100000;
		int maxY = -100000;

		for(int i=0; i<s.getPoints().size(); i++) {
			int xPoint = s.getPoints().get(i).getX();
			int yPoint = s.getPoints().get(i).getY();
			if(xPoint < minX) {
				minX = xPoint;
			} else if (xPoint > maxX) {
				maxX = xPoint;
			}
			if(yPoint < minY) {
				minY = yPoint;
			} else if (yPoint>maxY) {
				maxY = yPoint;
			}
		}

		return new Location(minX, minY, new Rectangle(maxX-minX, maxY-minY));
	}
}
